(function() {
   tinymce.PluginManager.add('ad_ninja_button', function( editor, url ) {
      editor.addButton( 'ad_ninja_button', {
         text: 'Ad Ninja',
         icon: false,
        
         onclick: function() {
            editor.windowManager.open( {
               title: 'Insert Page Ad',
               body: [
                  {
                     type: 'listbox',
                     name: 'listboxSidebarSelection',
                     label: 'Choose Widget Area:',
                     'values': [
                        {text: 'Middle Ad 1', value: 'middle-ad-1'},
                        {text: 'Middle Ad 2', value: 'middle-ad-2'},
                        {text: 'Middle Ad 3', value: 'middle-ad-3'}
                     ]
                  }
               ],
               onsubmit: function( e ) {
                  editor.insertContent( '[page_ad middlead="' + e.data.listboxSidebarSelection + '"]');
               }
            });
         }
                  
      });
   });
})();