jQuery(document).ready(function() { //start after HTML, images have loaded
 
    var InfiniteRotator =
    {
        init: function()
        {
            //initial fade-in time (in milliseconds) - 1 second
            var initialFadeIn = 1000;
 
            //interval between items (in milliseconds) - 10 seconds!
            var itemInterval = 10000;
 
            //cross-fade time (in milliseconds) - 2.5 seconds
            var fadeTime = 2500;
 
            //count number of items
            var numberOfItems = jQuery('.ad_ninja .widget').length;
 
            //set current item
            var currentItem = 0;
 
            //show first item
            jQuery('.ad_ninja .widget').eq(currentItem).fadeIn(initialFadeIn);
 
            //loop through the items
            var infiniteLoop = setInterval(function(){
                jQuery('.ad_ninja .widget').eq(currentItem).fadeOut(fadeTime);
 
                if(currentItem == numberOfItems -1){
                    currentItem = 0;
                }else{
                    currentItem++;
                }
                jQuery('.ad_ninja .widget').eq(currentItem).fadeIn(fadeTime);
 
            }, itemInterval);
        }
    };
 
    InfiniteRotator.init();
 
});