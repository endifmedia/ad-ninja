=== Ad Ninja ===
Contributors: ENDif Media
Requires at least: 3.7
Tested up to: 4.2.4
Stable tag: 1.2
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

WordPress plugin that allows multiple widgets to be placed in page/post content. Perfect tool for placing ads in your page content!

== Description ==

A simple and easy way to add widgets to your posts and pages.

ENJOY!

== Installation ==
1. Simply download the plugin and install.
2. Put a widget (or two) in one of the Ad Ninja widget places.
3. In the content editor of a page or post, click Ad Ninja in the kitchen sink.
4. Choose the Ad Ninja sidebar you want to use.
5. Click OK. 
6. The plugin will place the proper shortcode in the content area.
7. Save your post or page!

== Changelog ==

= 1.2 =
* Fixed bug in sidebar display
* Added Support for WP version 4.2.4

= 1.0 = 
* First release.