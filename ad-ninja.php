<?php
 /*
 Plugin Name: Ad Ninja
 Plugin URI: endif.media
 Description: Creates rotating widget place with shortcode: [rotate_ad sidebar_1="rotating-ad-1"]. Creates in-page ads with shortcode: [page_ad middlead="middle-ad-1"]
 Version: 1.2
 Author: Ethan Allen
 Author URI: endif.media
 License: GPLv2
 */
 
/**
 * If this file is called directly, abort.
 *
 */
if ( ! defined( 'WPINC' ) ) {
	die();
}

/**
 * Enqueues ad rotator JS in the site footer
 *
 * @version 1.1
 * @author ENDif Media
 */
function do_ad_ninja_scripts(){

    wp_enqueue_style( 'ad-ninjacss', plugins_url('ad-ninja/css/ad-ninjacss.css'));
    wp_enqueue_script( 'ad-ninjajs', plugins_url('ad-ninja/js/ad-ninjajs.js'), array( 'jquery' ), '20150514', true );

}
add_action('wp_footer', 'do_ad_ninja_scripts');

/**
 * Register ENDif Media theme widget areas.
 *
 * @version 1.1
 * @author ENDif Media
 */
function do_ad_ninja_widget_places() {

  register_sidebar( array(
    'name'          => __( 'Rotating Ad 1', 'ad-ninja' ),
    'id'            => 'rotating-ad-1',
    'description'   => __( 'rotating-ad-1', 'ad-ninja' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h1 class="widget-title">',
    'after_title'   => '</h1>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Rotating Ad 2', 'ad-ninja' ),
    'id'            => 'rotating-ad-2',
    'description'   => __( 'rotating-ad-2', 'ad-ninja' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h1 class="widget-title">',
    'after_title'   => '</h1>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Rotating Ad 3', 'ad-ninja' ),
    'id'            => 'rotating-ad-3',
    'description'   => __( 'rotating-ad-3', 'ad-ninja' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h1 class="widget-title">',
    'after_title'   => '</h1>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Rotating Ad 4', 'ad-ninja' ),
    'id'            => 'rotating-ad-4',
    'description'   => __( 'rotating-ad-4', 'ad-ninja' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h1 class="widget-title">',
    'after_title'   => '</h1>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Rotating Ad 5', 'ad-ninja' ),
    'id'            => 'rotating-ad-5',
    'description'   => __( 'rotating-ad-5', 'ad-ninja' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h1 class="widget-title">',
    'after_title'   => '</h1>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Middle Ad 1', 'ad-ninja' ),
    'id'            => 'middle-ad-1',
    'description'   => __( 'middle-ad-1', 'ad-ninja' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h1 class="widget-title">',
    'after_title'   => '</h1>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Middle Ad 2', 'ad-ninja' ),
    'id'            => 'middle-ad-2',
    'description'   => __( 'middle-ad-2', 'ad-ninja' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h1 class="widget-title">',
    'after_title'   => '</h1>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Middle Ad 3', 'ad-ninja' ),
    'id'            => 'middle-ad-3',
    'description'   => __( 'middle-ad-3', 'ad-ninja' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h1 class="widget-title">',
    'after_title'   => '</h1>',
  ) );

}
add_action( 'widgets_init', 'do_ad_ninja_widget_places' );

/**
 * Registers hooks to call button funtions.
 *
 * @version 1.1
 * @author ENDif Media
 */
function do_ad_ninja_button() {

    // check user permissions
    if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
        return;
    }
    // check if WYSIWYG is enabled
    if ( 'true' == get_user_option( 'rich_editing' ) ) {
        add_filter( 'mce_external_plugins', 'do_add_ad_ninja_plugin' );
        add_filter( 'mce_buttons', 'do_register_ad_ninja_button' );
    }

}
add_action('admin_head', 'do_ad_ninja_button');

/**
 * Create Ad Ninja tiny MCE button.
 *
 * @version 1.1
 * @author ENDif Media
 */
function do_add_ad_ninja_plugin( $plugin_array ) {

    $plugin_array['ad_ninja_button'] = plugins_url() . '/ad-ninja/js/ad-ninja-button.js';
    return $plugin_array;

}

/**
 * Register Ad Ninja tiny MCE button.
 *
 * @version 1.1
 * @author ENDif Media
 */
function do_register_ad_ninja_button( $buttons ) {

    array_push( $buttons, 'ad_ninja_button' );
    return $buttons;

}

/**
 * Creates inpage ad
 *
 * Use: [page_ad middlead=""]
 *
 * @version 1.2
 * @author ENDif Media
 */
 function do_midtext_ad($atts, $content = 'null'){
    extract( shortcode_atts(  array(
        'middlead' => '',
    ), $atts ) );

    if (is_active_sidebar($middlead)){

        ob_start();
        dynamic_sidebar($middlead);
        $widgets = ob_get_contents();
        ob_end_clean();

        return $widgets;

    } else {

        return '';
        
    }
 
 }
 add_shortcode('page_ad','do_midtext_ad');

/**
 * Creates rotating ad from inputted widget places 
 *
 * @version 1.1
 * @author ENDif Media
 */
 function do_ad_ninja_rotator($atts){
    extract( shortcode_atts(  array(
        'rotatead_1' => '',
        'rotatead_2' => '',
        'rotatead_3' => '',
        'rotatead_4' => '',
        'rotatead_5' => ''
    ), $atts ) ); //@version 1.2 - added closing tag ->?>
    
    <div class="ad_ninja">
        <?php dynamic_sidebar($rotatead_1); ?>
        <?php dynamic_sidebar($rotatead_2); ?>
        <?php dynamic_sidebar($rotatead_3); ?>
        <?php dynamic_sidebar($rotatead_4); ?>
        <?php dynamic_sidebar($rotatead_5); ?>
    </div>

<?php  //<- @version 1.2 - added opening tag  
}
add_shortcode('rotate_ad','do_ad_ninja_rotator');